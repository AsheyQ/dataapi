import json

import requests
from config import config


class Ozon:

    SEARCH_STAT_URL = 'https://data.ozon.ru/api/searchstat_analytics/queries_search'

    PRODUCT_ANALYSIS_URL = 'https://data.ozon.ru/api/analytics_what_to_sell/what_to_sell'

    cookies = {
        '__Secure-access-token': config.env['token']
        , 'Path': '/'
        , 'Expires': '2025-04-14T05:49:35.526Z'
    }

    def get_search_stats(self):
        with open('config/body/search_stat.json', 'r') as file:
            body = json.load(file)

        return requests.post(self.SEARCH_STAT_URL, json=body, cookies=self.cookies)

    def get_product_analysis(self):
        with open('config/body/product_analysis.json', 'r') as file:
            body = json.load(file)

        return requests.post(self.PRODUCT_ANALYSIS_URL, json=body, cookies=self.cookies)
