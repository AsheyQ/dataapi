import json
import os
from datetime import datetime

from api.ozon import Ozon


class Runner:
    client = Ozon()

    def run(self, search_stat, product_analysis):
        if not os.path.exists('dumps_temp'):
            os.makedirs('dumps_temp')

        if search_stat:
            if not os.path.exists('dumps_temp/search_stat'):
                os.makedirs('dumps_temp/search_stat')
            response_body = self.client.get_search_stats().json()
            with open(f'dumps_temp/search_stat/data_{datetime.now()}.json', 'w') as file:
                json.dump(response_body, file, indent=4)

        if product_analysis:
            if not os.path.exists('dumps_temp/product_analysis'):
                os.makedirs('dumps_temp/product_analysis')
            response_body = self.client.get_product_analysis().json()
            with open(f'dumps_temp/product_analysis/data_{datetime.now()}.json', 'w') as file:
                json.dump(response_body, file, indent=4)
