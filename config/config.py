import yaml


class Config:
    def __init__(self):
        with open('./config/config.yml', 'r') as file:
            self.data = yaml.safe_load(file)
        self.env = self.data['environment']

config = Config()