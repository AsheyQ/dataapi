from api.runner import Runner


def setup_params():
    args = {
        'search_stat': True,
        'product_analysis': True
    }

    return args


def execute(args):
    return Runner().run(**args)


if __name__ == '__main__':
    print(execute(setup_params()))
